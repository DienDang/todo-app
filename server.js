/**
 * Created by dangdien on 7/21/17.
 */

//SERVER SETUP

var express     = require("express");
var app         = express();
var mongoose    = require("mongoose");
var bodyParser  = require("body-parser");


//CONFIGURATION

mongoose.connect("mongodb://dangdien:Dien163@ds115573.mlab.com:15573/mytodoapp");

app.use(express.static("public"));
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

app.listen(process.env.PORT || 8080);
console.log("App listening on port 8080");

app.get('/', function (req, res) {
    res.sendfile('/index.html');
});

//Mongoose model
var Todo = mongoose.model('Todo', {
    text    :   String,
    status  :   Boolean     //TRUE if work is done
});


//CONTROLLERS

//Get all todos
var getAll = function (req, res) {
    Todo.find(function (err, todos) {

        //If there is an error occur
        if(err) {
            res.send(err);
        }

        //Return todo list as JSON
        res.json(todos);
    });
};


//Create a todo and send back all todos after creation
var createTodo = function (req, res) {
    Todo.create    ({
        text    : req.body.text,
        status  : false
    }, function (err, todos) {

        if(err) {
            res.send(err);
        }

        //If success => return all todos
        else getAll(req, res);
    });
};

//Delete a todo with id given by request params
var deleteTodo = function (req, res) {
  Todo.remove({
      _id: req.params.todo_id
  }, function (err, todos) {
      if(err) {
          res.send(err);
      }

      //If success => return all todos
      else getAll(req, res);
  })
};


//Update a todo given by request body
var updateTodo = function (req, res) {
    Todo.findByIdAndUpdate(req.body._id, req.body, function (err) {
        if(err) {
            console.log(err)
        }
        else getAll(req, res);
    });
};

//ROUTING

//Get all todos
app.get('/api/todos', getAll);


//Create a todo and send back all todos after creation
app.post('/api/todos',createTodo);

//Delete a todo with id given by request params
app.delete('/api/todos/:todo_id', deleteTodo);

//Update a todo given by request body
app.put('/api/todos', updateTodo);