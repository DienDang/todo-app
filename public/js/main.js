/**
 * Created by dangdien on 7/22/17.
 */
var todoApp = angular.module("todoApp", ['ngRoute']);
todoApp.controller("mainCtrl", ["$scope", "$http", function ($scope, $http) {
    $scope.todos = [];
    $http({
        method: 'GET',
        url: "/api/todos"
    }).then(function (res) {
        $scope.todos = res.data;
    });

    $scope.addNewTodo = function () {
        if ($scope.newTodo) {
            $http({
                method: 'POST',
                data: {
                    text: $scope.newTodo
                },
                url: "/api/todos"
            }).then(function (res) {
                if (res.data) {
                    $scope.todos = res.data;
                    $scope.newTodo = "";
                }
            });
        }
    };

    $scope.deleteTodo = function (todo) {
        if (todo) {
            $http({
                method: 'DELETE',
                url: "/api/todos/" + todo._id
            }).then(function (res) {
                if (res.data) {
                    $scope.todos = res.data;
                }
            });
        }
    };

    $scope.updateTodo = function (todo) {
        if (todo) {
            $http({
                method: 'PUT',
                data: todo,
                url: '/api/todos'
            }).then(function (res) {
                if (res.data) {
                    $scope.todos = res.data;
                }
            });
        }
    };


    todoApp.config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: "/index.html"
            })
            .otherwise({
                templateUrl: "/index.html"
            })
    });

}]);
